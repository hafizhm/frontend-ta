import React from 'react';
import dataDummy from 'assets/dummy/product.json'
import { Card, CardHeader, CardBody, Row, Col, Modal, ModalBody, ModalFooter, ModalHeader, Button, Input, Badge } from 'reactstrap';
import DataTable from 'react-data-table-component';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { bool } from 'prop-types';


export default props => {

    const [modal, setModal] = React.useState(false);
    const [banks, setBanks] = React.useState([]);
    const [newBanks, setNewBanks] = React.useState({
        name: "",
        owner: "",
        number: ""
    })

    const toggle = () => setModal(!modal);

    React.useEffect(() => {
        getBanksAPI();
    },[])

    const getBanksAPI = () => {
        axios.get('/bank-all')
        .then((result) => {
            setBanks(result.data.data);
        })
    }

        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Banks</CardHeader>
                    <CardBody>
                        <Button color="success" onClick={toggle} style={{width: 100, marginRight: 20}}>Add Bank</Button>
                        <Modal isOpen={modal} toggle={toggle}>
                            <ModalHeader>Add Bank</ModalHeader>
                                <ModalBody>
                                    <Row>
                                        <Col>
                                        <form>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>Bank</td>
                                                        <td style={{padding: 10}}>:</td>
                                                        <td>
                                                            <Input  />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Account Name</td>
                                                        <td style={{padding: 10}}>:</td>
                                                        <td>
                                                            <Input  />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Account Number</td>
                                                        <td style={{padding: 10}}>:</td>
                                                        <td>
                                                            <Input  />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{paddingTop: 20}}>
                                                            <label>Status<span style={{padding: "0px 20px"}}>:</span></label>
                                                            <input type="radio" name="check" value="ACTIVE"/>
                                                                <label for="active" style={{marginLeft: 5}}>Active</label>
                                                            <input type="radio" name="check" value="NOTACTIVE" style={{marginLeft: 25}}/>
                                                                <label for="notactive" style={{marginLeft: 5}}>NotActive</label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </form>
                                        </Col>
                                    </Row>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="success" style={{width: 100}}>Save</Button>
                                    <Button onClick={toggle} color="secondary" style={{width: 100, marginLeft: 10}}>Cancel</Button>
                                </ModalFooter>
                        </Modal>
                        <DataTable
                            noHeader={true}
                            data={banks}
                            striped={true}
                            columns={
                                [
                                    {
                                        name: 'Bank',
                                        selector: 'name',
                                        sortable: true,
                                    },
                                    {
                                        name: 'Account Name',
                                        selector: 'owner',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Account Number',
                                        selector: 'number',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Status',
                                        selector: 'status',
                                        sortable: true,
                                        cell: banks => (
                                            <div>
                                                {
                                                    banks.status === 1 ? (
                                                        <Badge style={{padding: "8px 19px"}} color="success">ACTIVE</Badge>
                                                    )
                                                    :
                                                    (
                                                        <Badge style={{padding: 8}} color="danger">NOTACTIVE</Badge>
                                                    )
                                                }
                                            </div>
                                        )
                                    },
                                    {
                                        name: '',
                                        selector: '',
                                        sortable: false,
                                        cell: banks => (
                                            <div>
                                                <Button color="primary">
                                                    <Link to={`/banks/edit/${banks.id}`} style={{color: "#fff", textDecoration: "none"}}>Edit</Link>
                                                </Button>
                                            </div>
                                        )
                                    }
                                ]}
                                pagination={true}
                            />
                    </CardBody>
                </Card>
            </div>
        )
    }

// class Banks extends React.Component{

//     constructor(props) {
//         super(props);
//         this.state = {
//             modal : false,
//             banks: [],
//             newBanks: {
//                 name: "",
//                 number: "",
//                 owner: ""
//             }
//         }

//         this.handleChange = this.handleChange.bind(this);
//         this.toggle = this.toggle.bind(this);
//     }


//     toggle = () => {
//         this.setState({
//             modal: !this.state.modal
//         })
//     }

//     componentDidMount() {
//         this.getBanksAPI();
//     }
    
//     getBanksAPI = () => {
//         axios.get('/bank-all')
//         .then((result) => {
//             this.setState({
//                 banks: result.data.data
//             })
//         })
//     }

//     handleChange = (e) => {
//         this.setState({
//             newBanks: e.target.value
//         })
//     }

//     handleSubmit = (e) => {
//         const { banks, newBanks } = this.state;
//         const token = localStorage.getItem("token");
//         alert('A name was submitted: ' + newBanks);
//         e.preventDefault();
//         axios.post('/bank', banks, {
//             headers: {
//                 "Access-Control-Allow-Origin": "*",
//                 "Access-Control-Allow-Headers": "Authorization",
//                 "Authorization": token,
//                 "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
//                 "Content-Type": "application/json"
//             }
//         })
//         .then((result) => {
//             this.setState({
//                 banks: result.data.data
//             })
//         }).catch(error => {
//             console.log(`error! `, error);
//         })
//     }

    


// render(){

// }

// export default Banks;
