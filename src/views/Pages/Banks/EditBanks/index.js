import React from 'react';
import dataDummy from 'assets/dummy/product.json'
import { Button, Card, CardHeader, CardBody, Row, Col, Input } from 'reactstrap';
import axios from 'axios';

export default props => {

    const [banks, setBanks] = React.useState([]);
        // const {bankFile} = this.state;
        // const id = this.props.match.params.id
        // const data = bankFile.filter(dataItem => dataItem.id == id)
        // const banks = data[0]

        React.useEffect(()=>{
            getPostAPI();
        },[])

        const getPostAPI = () =>{
            axios.get('/bank-all')
            .then((result) =>{
                setBanks(result.data.data);
            })
        }

        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Edit Product Nomenclature</CardHeader>
                    <CardBody>
                        <Row>
                            <Col>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Bank</td>
                                            <td style={{padding: 10}}>:</td>
                                            <td>
                                                <Input placeholder={banks.name}/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Name</td>
                                            <td style={{padding: 10}}>:</td>
                                            <td>
                                                <Input placeholder={banks.owner}/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td style={{padding: 10}}>:</td>
                                            <td>
                                                <Input style={{width: 500}} placeholder={banks.number}/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <div>
                                                <input type="radio" name="check" value="ACTIVE" checked={banks.status === "ACTIVE"}/>
                                                    <label htmlFor="active" style={{marginLeft: 5}}>Active</label>
                                                <input type="radio" name="check" value="NOTACTIIVE" checked={banks.status === "NOTACTIVE"} style={{marginLeft: 5}}/>
                                                    <label htmlFor="notactive" style={{marginLeft: 5}}>NotActive</label>
                                            </div>
                                        </tr>
                                    </tbody>
                                </table>
                                <Button color="success" onClick={() => alert('sukses')} style={{border: "none", marginTop:25}}>Save</Button>
                                <Button color="secondary" style={{marginLeft: 10, marginTop: 25, border: "none"}}>Cancel</Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }
