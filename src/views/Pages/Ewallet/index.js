import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Badge, Row, Col, Input } from 'reactstrap';
import dataDummy from 'assets/dummy/product.json';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default props => {
  
  const [search, setSearch] = React.useState("");
  const [wallet, setWallet] = React.useState([]);
  const [filterSearch, setFilterSearch] = React.useState("");

  React.useEffect(() => {
    getWalletAPI();
  },[])

  const getWalletAPI = () => {
    axios.get('/wallet?page=1&size=1000')
    .then((result) => {
      setWallet(result.data.data.content);
    })
  }


  return(
      <div className="animated fadeIn">
          <Card>
              <CardHeader>e-Wallet</CardHeader>
              <CardBody>
                  <Row>
                      <Col md={6}>
                          <Input value={search} onChange={e => {
                            const firstData = getWalletAPI();
                            const userWallet = wallet.filter(data => {
                              return data.user.firstName.toLowerCase().includes(e.target.value.toLowerCase());
                            });
                            if(e.target.value === ''){
                              setWallet(firstData)
                            } else {
                              setWallet(userWallet);
                            }
                            setSearch(e.target.value);
                          }} 
                          placeholder="Search Name..."  />
                      </Col>
                  </Row>
                    <DataTable
                          data={wallet}
                          noHeader={true}
                          columns={
                            [
                              {
                                name: <b>Customer Accounts</b>,
                                selector: 'user.email',
                                sortable: true,
                              },
                              {
                                name: <b>Name</b>,
                                selector: 'user.firstName',
                                sortable: false,
                                cell: wallet => {
                                  return `${wallet.user.firstName} ${wallet.user.lastName}`
                                }
                              },
                              {
                                name: <b>Saldo</b>,
                                selector: 'user.saldo',
                                sortable: false,
                                cell: wallet => (
                                  <div>Rp. {wallet.user.saldo}</div>
                                )
                              },
                              {
                                name: '',
                                selector: 'year',
                                sortable: true,
                                cell : wallet => (
                                  <Button color="primary" style={{border: "none"}}>
                                    <Link style={{color: "#fff", textDecoration: "none"}} to={`/product/${dataDummy.id}`}>Detail</Link>
                                  </Button>
                                )
                              },
                          ]}
                          pagination={true}
                      />
              </CardBody>
          </Card>
      </div>
  )
}

