import React from 'react';
import { Card, CardHeader, CardBody, Row, Col, Button } from 'reactstrap';

class DetailOrders extends React.Component {

    constructor(props) {
        super(props);
        console.log(this.props)
    }


    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col sm={12}>
                        <Card>
                            <CardHeader>Detail Orders</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col sm={12} style={{ marginBottom: 15, float: "left" }}>
                                        <Row>
                                            <Col sm={6}>
                                                <h5>ORDER ID A1101107</h5>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>lorem Ipsum</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </Col>
                                            <Col sm={6}>
                                                <Button color="danger" style={{ float: "right" }} onClick={() => alert('edit ')}>Cancel Orders</Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col sm={12}>
                                        <Row>
                                            <Col sm={12} >
                                                <Row>
                                                    <Col md={2}>
                                                        <h5>Customer Account</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                    <Col md={2}>
                                                        <h5>Customer Name</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                    <Col md={2}>
                                                        <h5>Customer Phone</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                    <Col md={2}>
                                                        <h5>Total Payment</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                    <Col md={2}>
                                                        <h5>Payment Method</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                    <Col md={2}>
                                                        <h5>Shipping Method</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col sm={12}>
                        <Row>
                            <Col sm={12} md={8}>
                                <Card>
                                    <Card>
                                        <CardBody>
                                            <Row>
                                                <Col sm={12} style={{ marginBottom: 15, float: "left" }}>
                                                    <Row>
                                                        <Col sm={6}>
                                                            <h5>DELIVERY CODE A1101107</h5>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>lorem Ipsum</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <Button color="primary" style={{ float: "right" }} onClick={() => alert('edit ')}>Edit</Button>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col sm={12}>
                                                    <Row>
                                                        <Col sm={12} >
                                                            <Row>
                                                                <Col md={6} style={{ marginBottom: 10, float: "left" }}>
                                                                    <h7>SHIPPING ADDRESS</h7>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Lorem Ipsum Dolor sit amet</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </Col>
                                                                <Col md={6} style={{ float: "right" }}>
                                                                    <h7>PIC / PENERIMA</h7>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Lorem Ipsum Dolor sit amet</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col sm={12} style={{ marginBottom: 10, float: "left" }} >
                                                            <Row>
                                                                <Col md={6}>
                                                                    <h7>SHIPPING ADDRESS</h7>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Lorem Ipsum Dolor sit amet</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </Col>
                                                                <Col md={6} style={{ float: "right" }}>
                                                                    <h7 >PIC / PENERIMA</h7>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Lorem Ipsum Dolor sit amet</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col sm={12} style={{ marginBottom: 10, float: "left" }} >
                                                            <Row>
                                                                <Col md={6}>
                                                                    <h7>SHIPPING ADDRESS</h7>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Lorem Ipsum Dolor sit amet</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </Col>
                                                                <Col md={6} style={{ float: "right" }}>
                                                                    <h7 >PIC / PENERIMA</h7>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Lorem Ipsum Dolor sit amet</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Card>
                            </Col>
                            <Col sm={12} md={4}>
                                <Row>
                                    <Col sm={12}>
                                        <Card>
                                            <CardBody>
                                                <Row>
                                                    <Col sm={12}>
                                                        <h5>Payment Confirm</h5>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Bank Account Name</td>
                                                                    <td style={{ padding: "0 5px" }}>:</td>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Bank Source</td>
                                                                    <td style={{ padding: "0 5px" }}>:</td>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Account Number</td>
                                                                    <td style={{ padding: "0 5px" }}>:</td>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Bank Destination</td>
                                                                    <td style={{ padding: "0 5px" }}>:</td>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Amount</td>
                                                                    <td style={{ padding: "0 5px" }}>:</td>
                                                                    <td>Lorem Ipsum Dolor sit amet</td>
                                                                </tr>
                                                                <Row>
                                                                    <Col sm={6}>
                                                                        <Button color="success" style={{}} onClick={() => alert('edit ')}>Verify</Button>
                                                                    </Col>
                                                                    <Col sm={6}>
                                                                        <Button color="danger" style={{}} onClick={() => alert('edit ')}>Reject</Button>
                                                                    </Col>
                                                                </Row>

                                                            </tbody>
                                                        </table>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col sm={12}>
                                        <Card>Bawah</Card>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        )
    }

}

export default DetailOrders;