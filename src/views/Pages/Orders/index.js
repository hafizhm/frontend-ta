import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Badge, Nav, NavItem, NavLink, TabContent, TabPane, Row, Col, Input } from 'reactstrap';
import dataDummy from 'assets/dummy/orders.json';
import { Link } from 'react-router-dom';


class Orders extends React.Component {

    constructor(props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
        this.state = {
            activeTab: "1",
            filterSearch: ""
        };
    }

    handleToggle = (tab) => {
        const { activeTab } = this.state
        this.setState({
            activeTab: tab
        });
    }

    handleFilterActive = () => {
        const data = dataDummy.filter(dataItem => dataItem.statusOrder === "open")
        return data;
    }

    handleFilterNotActive = () => {
        const dataOrders = this.handleFilterQuery()
        const data = dataOrders.filter(dataItem => dataItem.statusOrder === "closed")
        return data;
    }

    handleOnChange = (e) => {
        this.setState({
            filterSearch: e.target.value
        })
    }

    handleFilterQuery = () => {
        const { filterSearch } = this.state
        const dataOrders = dataDummy.filter(
            dataItem => dataItem.orderId.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1 || dataItem.customerAccount.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1)
        return dataOrders
    }


    render() {
        const productActive = this.handleFilterActive();
        const productNotActive = this.handleFilterNotActive();
        const { activeTab } = this.state;
        return (
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Orders</CardHeader>
                    <CardBody>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    active={activeTab === "1"}
                                    onClick={() => this.handleToggle("1")}>
                                    Pending
                          </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink active={activeTab === "2"}
                                    onClick={() => this.handleToggle("2")}>
                                    ALL
                          </NavLink>
                            </NavItem>
                        </Nav>
                        <Row>
                            <Col md={4}>
                                <Input placeholder="Search..." style={{ margin: 10 }} onChange={this.handleOnChange} />
                            </Col>
                        </Row>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <DataTable
                                    data={productNotActive}
                                    noHeader={true}
                                    columns={
                                        [
                                            {
                                                name: 'Order ID',
                                                selector: 'orderId',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Customer Account',
                                                selector: 'customerAccount',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Total Payment',
                                                selector: 'totPayment',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Order Date',
                                                selector: 'orderDate',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Payment Method',
                                                selector: 'paymentMethod',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Status',
                                                selector: 'year',
                                                sortable: false,
                                                cell: dataDummy => (
                                                    <Badge color="primary" style={{ padding: 8 }}>{dataDummy.status}</Badge>
                                                )
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (
                                                    <Button>
                                                        <Link to={`/orders/${dataDummy.id}`}>Detail</Link>
                                                    </Button>
                                                )
                                            },
                                        ]}
                                    pagination={true}
                                />
                            </TabPane>
                            <TabPane tabId="2">
                                <DataTable
                                    data={productActive}
                                    noHeader={true}
                                    columns={
                                        [
                                            {
                                                name: 'Order ID',
                                                selector: 'orderId',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Customer Account',
                                                selector: 'customerAccount',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Total Payment',
                                                selector: 'totPayment',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Order Date',
                                                selector: 'orderDate',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Payment Method',
                                                selector: 'paymentMethod',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Status',
                                                selector: 'year',
                                                sortable: false,
                                                cell: dataDummy => (
                                                    <Badge color="primary" style={{ padding: 8 }}>{dataDummy.status}</Badge>
                                                )
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (
                                                    <Button>
                                                        <Link to={`/orders/${dataDummy.id}`}>Detail</Link>
                                                    </Button>
                                                )
                                            },
                                        ]}
                                    pagination={true}
                                />
                            </TabPane>
                        </TabContent>
                    </CardBody>
                </Card>
            </div>
        )
    }

}

export default Orders;