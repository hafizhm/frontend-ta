import React from 'react';
import { CardHeader, CardBody, Card, Row, Col, Input, Button } from 'reactstrap';
import axios from 'axios';

export default () => {
    
    const [product, setProduct] = React.useState([]);
    const [category, setCategory] = React.useState([]);

    React.useEffect(() => {
        getProductNomenclatureAPI();
        getProductCategoryAPI();
    },[])

    const getProductNomenclatureAPI = () => {
        axios.get('/product-nomenclature?page=0&size=1000')
        .then((result) => {
            setProduct(result.data.data.content)
        })
    }

    const getProductCategoryAPI = () => {
        axios.get('/product-category-exclusive')
        .then((result) => {
            setCategory(result.data)
        })
    }

    const handleSubmitProduct = (event) => {
        setProduct({
            ...product,
            [event.target.name] : event.target.value
        });
    }

    // const {name, description, individualPrice, businessPrice, 
    //     privilegePrice, highestPrice, lowestPrice, 
    //     discount, status, reference} = product;


        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Add Product</CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm={12} md={8}>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><b>SKU ID</b></td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input disabled style={{marginBottom: 10}} onChange={()=> handleSubmitProduct()}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Nomenclature Category</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                        {product.filter(p => p.classification === "CATEGORY").map(item => (
                                                            <option
                                                            key={item.id}
                                                            value={item.classification === "CATEGORY"}
                                                            >
                                                            {item.description}
                                                            </option>
                                                        ))}
                                                    </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                    {product.filter(p => p.classification === "TYPE").map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.classification === "TYPE"}
                                                        >
                                                        {item.description}
                                                        </option>
                                                    ))}
                                                </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Group</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                    {product.filter(p => p.classification === "GROUP").map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.classification === "GROUP"}
                                                        >
                                                        {item.description}
                                                        </option>
                                                    ))}
                                                </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Product</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                    {product.filter(p => p.classification === "PRODUCT").map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.classification === "PRODUCT"}
                                                        >
                                                        {item.description}
                                                        </option>
                                                    ))}
                                                </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>UOM</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                    {product.filter(p => p.classification === "UOM").map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.classification === "UOM"}
                                                        >
                                                        {item.description}
                                                        </option>
                                                    ))}
                                                </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pack Size</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                    {product.filter(p => p.classification === "PACK_SIZE").map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.classification === "PACK_SIZE"}
                                                        >
                                                        {item.description}
                                                        </option>
                                                    ))}
                                                </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Product Name</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input  style={{marginBottom: 10, border: "1px solid #e4e7ea"}}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Category</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="select" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}>
                                                <option selected disabled> </option>
                                                    {category.filter(c => c.exclusive === true).map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.exclusive === true}
                                                        >
                                                        {item.name}
                                                        </option>
                                                    ))}
                                                </Input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Image URL</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10, border: "1px solid #e4e7ea"}}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pack Size (gram)</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10, border: "1px solid #e4e7ea"}} placeholder="gram"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Description</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                <Input type="textarea" style={{marginBottom: 10, border: "1px solid #e4e7ea"}}/>
                                                </td>
                                            </tr>
                                            <h4 style={{fontWeight: "bold"}}>PRICE LEVELING</h4>
                                            <tr>
                                                <td>Individual</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Business</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Privilege</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Discount</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Harga Eceran Terendah</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Harga Eceran Tertinggi</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <input type="radio" value="ACTIVE" name="status"/>
                                                    <label style={{margin: "0px 5px"}}>Active</label>
                                                    <input type="radio" value="NOTACTIVE" name="status"/>
                                                    <label style={{margin: "0px 5px"}}>NotActive</label>
                                                    {/* <UncontrolledDropdown setActiveFromChild>
                                                    <DropdownToggle style={{marginTop: 10, marginBottom: 10}} color="danger" caret>
                                                        Ubah Status
                                                    </DropdownToggle>
                                                    <DropdownMenu>
                                                        <DropdownItem>ACTIVE</DropdownItem>
                                                        <DropdownItem>NOTACTIVE</DropdownItem>
                                                    </DropdownMenu>
                                                    </UncontrolledDropdown> */}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Reference</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} onChange={e => handleSubmitProduct(e)} />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div>
                                        <Button style={{marginTop: 10, width: 340}} color="success" onClick={() => alert('sukses')}>Simpan</Button>
                                    </div>
                                </Col>
                            </Row>
                        </CardBody>
                </Card>
            </div>
        )
    }
