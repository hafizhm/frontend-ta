import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Badge, Nav, NavItem, NavLink, TabContent, TabPane, Col, Row, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default props => {

  const [search, setSearch] = React.useState("");
  const [activeTab, setActiveTab] = React.useState("1");
  const [product, setProduct] = React.useState([]);
  const [checked, setChecked] = React.useState([]);

  React.useEffect(() => {
    getProductAPI();
  },[])
  
  const getProductAPI = () =>{
    axios.get('/product-all?page=1&size=1240&q=&status=&category=')
    .then((result) =>{
      setProduct(result.data.data.content)
    })
  }
  
  const productNotActive = product.filter(items => items.status === "NOTACTIVE");
  
  const productActive = product.filter(items => items.status === "ACTIVE");

  const handleToggle = (tab) => {
    setActiveTab(tab);
  }

    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>Product</CardHeader>
          <CardBody>
            <Nav tabs>
              <NavItem>
                <NavLink
                  active={activeTab === "1"}
                  onClick={() => handleToggle("1")}>
                  NotActive
                          </NavLink>
              </NavItem>
              <NavItem>
                <NavLink active={activeTab === "2"}
                  onClick={() => handleToggle("2")}>
                  Active
                          </NavLink>
              </NavItem>
            </Nav>
            <Row>
              <Col md={6}>
                <Input placeholder="Search..." style={{ margin: 10 }} value={search} onChange={e => {
                  const firstData = getProductAPI();
                  const dataProduct = product.filter(data => {
                    return data.name.toLowerCase().includes(e.target.value.toLowerCase());
                  });
                  setSearch(e.target.value)
                  setProduct(dataProduct)
                   
                }}
                />
              </Col>
              <Col md={6}>
                <Button style={{ float: "right", margin: 10 }} color="success">
                  <Link style={{color: "#fff", textDecoration: "none"}} to={`/product/add`}>Add Product</Link>
                </Button>
              </Col>
            </Row>
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
                <DataTable
                  data={productNotActive}
                  noHeader={true}
                  columns={
                    [
                      {
                        name: 'SKU ID',
                        selector: 'code',
                        sortable: true,
                      },
                      {
                        name: 'Product Name',
                        selector: 'name',
                        sortable: false,
                      },
                      {
                        name: 'Category',
                        selector: 'category.name',
                        sortable: true,
                      },
                      {
                        name: 'Status',
                        selector: 'year',
                        sortable: true,
                        cell: product => (
                          <Badge color="secondary" style={{ padding: 8 }}>{product.status}</Badge>
                        )
                      },
                      {
                        name: 'Approval',
                        selector: 'approval',
                        sortable: true,
                      },
                      {
                        name: '',
                        selector: 'year',
                        sortable: true,
                        cell: product => (
                          <Button color="primary" style={{border: "none"}}>
                            <Link style={{color: "#fff", textDecoration: "none"}} to={`/product/${product.id}`}>Detail</Link>
                          </Button>
                        )
                      },
                    ]}
                  pagination={true}
                />
              </TabPane>
              <TabPane tabId="2">
                <DataTable
                  data={productActive}
                  noHeader={true}
                  columns={
                    [
                      {
                        name: 'SKU ID',
                        selector: 'code',
                        sortable: true,
                      },
                      {
                        name: 'Product Name',
                        selector: 'name',
                        sortable: false,
                      },
                      {
                        name: 'Category',
                        selector: 'category.name',
                        sortable: true,
                      },
                      {
                        name: 'Status',
                        selector: 'year',
                        sortable: true,
                        cell: product => (
                          <div>
                            {
                              product.status === "ACTIVE" ? (
                                <Badge color="success" style={{ padding: 8 }}>{product.status}</Badge>
                              )
                                :
                              (
                                <Badge color="secondary" style={{ padding: 8 }}>{product.status}</Badge>
                              )
                            }
                          </div>
                        )
                      },
                      {
                        name: 'Approval',
                        selector: 'approval',
                        sortable: true,
                      },
                      {
                        name: '',
                        selector: 'year',
                        sortable: true,
                        cell: product => (
                          <Button color="primary" style={{border: "none"}}>
                            <Link style={{color: "#fff", textDecoration: "none"}} to={`/product/${product.id}`}>Detail</Link>
                          </Button>
                        )
                      },
                    ]}
                  pagination={true}
                />
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>
      </div>
    )
  }

