import React from 'react';
import { CardHeader, CardBody, Card, Row, Col, Input, Button, DropdownMenu, DropdownItem, DropdownToggle, UncontrolledDropdown, Label } from 'reactstrap';
import dataDummy from 'assets/dummy/product.json';
import axios from 'axios';

export default props => {
  
  const [product, setProduct] = React.useState([]);

  React.useEffect(() => {
    getProductAPI();
  },[])
  
  const getProductAPI = () =>{
    const { match } = props;
    let {id} = match.params;
    axios.get(`/product/${id}`)
    .then((result) =>{
      setProduct(result.data)
    })
  }
  
  const goBack = () => {
      props.history.goBack();
  }
        
        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Edit Product</CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm={12} md={6}>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>SKU ID</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} placeholder={product ? product.code : ""} disabled/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Nomenclature Category</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product.category ? product.name : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Product Name</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.name : ""} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pack Size</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.code : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Description</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input type="textarea" style={{marginBottom: 10}} placeholder={product ? product.description : ""}/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </Col>
                                <Col sm={12} md={6}>
                                <h5>PRICE LEVELING</h5>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Individual</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.individualPrice : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Business</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.businessPrice : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Privilege</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.privilegePrice : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Discount</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.discount : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Harga Eceran Terendah</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.lowestPrice : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Harga Eceran Tertinggi</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.highestPrice : ""}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} defaultValue={product ? product.status : ""}/>
                                                    {/* <UncontrolledDropdown setActiveFromChild>
                                                    <DropdownToggle style={{marginTop: 10, marginBottom: 10}} color="danger" caret>
                                                        Ubah Status
                                                    </DropdownToggle>
                                                    <DropdownMenu>
                                                        <DropdownItem>ACTIVE</DropdownItem>
                                                        <DropdownItem>NOTACTIVE</DropdownItem>
                                                    </DropdownMenu>
                                                    </UncontrolledDropdown> */}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Reference</td>
                                                <td style={{padding: "0 10px"}}>:</td>
                                                <td>
                                                    <Input style={{marginBottom: 10}} />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <Button style={{marginTop: 10, width: "30%", marginRight: 16}} color="success">Save</Button>
                                    <Button style={{marginTop: 10, width: "30%", marginLeft: 16}} color="secondary" onClick={goBack}>Cancel</Button>
                                </Col>
                            </Row>
                        </CardBody>
                </Card>
            </div>
        )
    }