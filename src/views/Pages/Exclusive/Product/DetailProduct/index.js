import React from 'react';
import { Card, CardHeader, CardBody, Row, Col, Button } from 'reactstrap';
import dataDummy from 'assets/dummy/product.json';
import { Link } from 'react-router-dom';
import axios from 'axios';

class DetailProduct extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            product: [],
        }
    }

    componentDidMount(){
        this.getProductAPI();
    }

    
    getProductAPI = () => {
        const id = this.props.match.params.id;
        console.log(id)
        axios.get(`/product/${id}`)
        .then((result) => {
            console.log(result)
            this.setState({
                product: result.data
            })
        })
    }

    handleRemove = () => {
        const id = this.props.match.params.id;
        axios.delete(`/product/${id}`)
        .then((result) =>{
            this.getProductAPI()
        })
    }
    
    
    render(){
        const {product} = this.state

        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Detail Product</CardHeader>
                    <CardBody>
                        <Row>
                            <Col sm={12} md={6}>
                                <h5>SKU ID <b>{product ? product.code : ""}</b></h5>
                                <table>
                                    <tbody>
                                        <tr >
                                            <td>Nomenclature Category</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td className="pCategory">{product.category ? product.category.name : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Product Name</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td className="pName">{product ? product.name : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Pack Size</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td className="pPack">{product ? product.name : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td className="pDesc">{product ? product.description : ""}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col>
                            <Col sm={12} md={6}>
                                <Button color="danger" style={{float: "right"}} onClick={this.handleRemove}>
                                    <i className="icon-close"/>
                                    <span> Delete</span>
                                </Button>
                            <h5>PRICE LEVELING</h5>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Individual</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.individualPrice : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Business</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.businessPrice : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Privilege</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.privilegePrice : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.discount : ""}%</td>
                                        </tr>
                                        <tr>
                                            <td>Harga Eceran Terendah</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.lowestPrice : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Harga Eceran Tertinggi</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.highestPrice : ""}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td><b>{product ? product.status : ""}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Reference</td>
                                            <td style={{padding: "0 10px"}}>:</td>
                                            <td>{product ? product.reference : ""}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col>
                            <Col sm={12} md={6}>
                                <Link style={{color: "#fff", textDecoration: "none"}} to={`/product/edit/${product.id}`}>
                                    <Button color="primary" >Edit</Button>
                                </Link>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }

}

export default DetailProduct;