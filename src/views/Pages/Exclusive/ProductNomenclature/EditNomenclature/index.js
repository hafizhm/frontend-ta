import React, { Component } from 'react';
import { CardHeader, CardBody, Card, Row, Col, Input, Button } from 'reactstrap';
import dataDummy from 'assets/dummy/product.json';

class EditNomenclature extends Component {

    constructor(props) {
        super(props);
        this.state = {
            product: dataDummy
        }

        this.goBack = this.goBack.bind(this);
    }
    
    goBack = () =>{
        this.props.history.goBack();
    }

    render(){

        const {product} = this.state
        const id = this.props.match.params.id
        const data = product.filter(dataItem => dataItem.id == id)
        const editProd = data[0]
        console.log(editProd)

        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Edit Product Nomenclature</CardHeader>
                    <CardBody>
                        <Row>
                            <Col>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Classification</td>
                                            <td style={{padding: 10}}>:</td>
                                            <td>
                                                <Input placeholder={editProd.nomenclatureCategory}/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Code</td>
                                            <td style={{padding: 10}}>:</td>
                                            <td>
                                                <Input disabled placeholder={editProd.skuId}/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td style={{padding: 10}}>:</td>
                                            <td>
                                                <Input type="textarea" style={{width: 500}} placeholder={editProd.status}/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <Button color="success" onClick={() => alert('sukses')} style={{border: "none", marginTop:25}}>Save</Button>
                                <Button color="secondary" onClick={this.goBack} style={{marginLeft: 10, marginTop: 25, border: "none"}}>Cancel</Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }
}

export default EditNomenclature;