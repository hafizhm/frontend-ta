import React from 'react';
import { Card, CardHeader, CardBody, DropdownMenu, DropdownItem, Button, Badge, Modal, ModalHeader, ModalBody, ModalFooter, Input, Row, Col } from 'reactstrap';
import DataTable, { createTheme } from 'react-data-table-component';
import { Link } from 'react-router-dom';
import dataDummy from 'assets/dummy/product.json'

class ProductNon extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            product: dataDummy
        }

        this.toggleAdd = this.toggleAdd.bind(this);
    }

    toggleAdd = () => {
        this.setState({
            modal: !this.state.modal,
        })
    }

    handleProd = () => {
        const dataProd = dataDummy.filter(dataItem => dataItem.reference === "false")
        return dataProd;
    }

    render(){
        const {product} = this.state;
        const prodi = product.filter(dataItem => dataItem.reference === "false")

        const dataArchive = this.handleProd();
        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Product Nomenclature</CardHeader>
                    <CardBody>
                        <Button onClick={this.toggleAdd} color="success" style={{width: 100}}>Add</Button>
                            <Modal isOpen={this.state.modal} toggle={this.toggleAdd}>
                            <ModalHeader toggle={this.toggleAdd}>Add Nomenclature</ModalHeader>
                            <ModalBody>
                                <Row>
                                    <Col>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Classification</td>
                                                    <td style={{padding: 10}}>:</td>
                                                    <td>
                                                        <Input/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Code</td>
                                                    <td style={{padding: 10}}>:</td>
                                                    <td>
                                                        <Input/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td style={{padding: 10}}>:</td>
                                                    <td>
                                                        <Input type="textarea"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="success" onClick={this.toggleAdd}>Save</Button>
                                <Button color="secondary" onClick={this.toggleAdd}>Cancel</Button>
                            </ModalFooter>
                            </Modal>
                        <DataTable
                            data={dataArchive}
                            noHeader={true}
                            striped={true}
                            columns={
                                [
                                    {
                                        name: 'Classification',
                                        selector: 'nomenclatureCategory',
                                        sortable: true,
                                    },
                                    {
                                        name: 'Code',
                                        selector: 'skuId',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Description',
                                        selector: 'status',
                                        sortable: false,
                                        cell: dataDummy => (
                                            <div>
                                                {
                                                    dataDummy.status === "ACTIVE" ? (
                                                        <Badge color="success" style={{padding: "8px 19px"}}>{dataDummy.status}</Badge>
                                                    )
                                                    :
                                                    (
                                                        <Badge color="secondary" style={{padding: 8}}>{dataDummy.status}</Badge>
                                                    )
                                                }
                                            </div>
                                        )
                                    },
                                    {
                                        name: '',
                                        selector: '',
                                        sortable: false,
                                        cell: dataDummy => (
                                            <div>
                                                <Button color="primary" style={{border: "none"}}>
                                                    <Link style={{color: "#fff", textDecoration: "none"}} to={`/productnomenclature/${dataDummy.id}/edit`}>Edit</Link>
                                                </Button>
                                            </div>
                                        )
                                    }
                                ]}
                            pagination={true}
                            />
                    </CardBody>
                </Card>
            </div>
        )
    }

}

export default ProductNon;