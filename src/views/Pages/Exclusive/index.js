import Product from './Product';
import AddProduct from './Product/AddProduct';
import DetailProduct from './Product/DetailProduct';
import EditProduct from './Product/DetailProduct/EditProduct';
import ProductNomenclature from './ProductNomenclature';
import EditNomenclature from './ProductNomenclature/EditNomenclature'
import Warehouse from './Warehouse';

export {
  Product, ProductNomenclature, Warehouse, DetailProduct, EditProduct, AddProduct, EditNomenclature
};