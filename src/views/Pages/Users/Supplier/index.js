import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Badge, Row, Col, Input } from 'reactstrap';
import dataDummy from 'assets/dummy/supplier.json';
import { Link } from 'react-router-dom';

class Supplier extends React.Component {

    constructor(props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
        this.state = {
            activeTab: "1",
            filterSearch: ""
        };
    }

    handleToggle = (tab) => {
        this.setState({
            activeTab: tab
        });
    }

    handleFilterActive = () => {
        const dataSupplier = this.handleFilterQuery()
    }

    handleFilterQuery = () => {
        const { filterSearch } = this.state
        const dataSupplier = dataDummy.filter(
            dataItem => dataItem.email.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1 || dataItem.fullname.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1)
        return dataSupplier
    }


    render() {
        return (
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Supplier</CardHeader>
                    <CardBody>
                        <Row>
                            <Col md={6}>
                                <Input placeholder="Search Name..."></Input>
                            </Col>
                        </Row>
                        <DataTable
                            data={dataDummy}
                            noHeader={true}
                            columns={
                                [
                                    {
                                        name: 'Email',
                                        selector: 'email',
                                        sortable: true,
                                    },
                                    {
                                        name: 'Full Name',
                                        selector: 'fullname',
                                        sortable: true,
                                    },
                                    {
                                        name: 'Deposit',
                                        selector: 'deposit',
                                        sortable: true,
                                    },

                                ]}
                            pagination={true}
                        />
                    </CardBody>
                </Card>
            </div>
        )
    }
}
export default Supplier;