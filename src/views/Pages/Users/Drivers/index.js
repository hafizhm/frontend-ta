import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Badge, Nav, NavItem, NavLink, TabContent, TabPane, Row, Col, Input } from 'reactstrap';
import dataDummy from 'assets/dummy/drivers.json';
import { Link } from 'react-router-dom';


class Drivers extends React.Component {

    constructor(props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
        this.state = {
            activeTab: "1",
            filterSearch: ""
        };
    }

    handleToggle = (tab) => {
        const { activeTab } = this.state
        this.setState({
            activeTab: tab
        });
    }

    handleFilterActive = () => {
        const dataDrivers = this.handleFilterQuery()
        const data = dataDummy.filter(dataItem => dataItem.statusDriver === "active")
        return data;
    }

    handleFilterNotActive = () => {
        const data = dataDummy.filter(dataItem => dataItem.statusDriver === "pending")
        return data;
    }

    handleOnChange = (e) => {
        this.setState({
            filterSearch: e.target.value
        })
    }

    handleFilterQuery = () => {
        const { filterSearch } = this.state
        const dataDrivers = dataDummy.filter(
            dataItem => dataItem.email.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1 || dataItem.name.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1)
        return dataDrivers
    }



    render() {
        const productActive = this.handleFilterActive();
        const productNotActive = this.handleFilterNotActive();
        const { activeTab } = this.state;
        return (
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Drivers</CardHeader>
                    <CardBody>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    active={activeTab === "1"}
                                    onClick={() => this.handleToggle("1")}>
                                    Pending
                          </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink active={activeTab === "2"}
                                    onClick={() => this.handleToggle("2")}>
                                    ALL
                          </NavLink>
                            </NavItem>
                        </Nav>
                        <Row>
                            <Col md={3}>
                                <Input placeholder="Search..." style={{ margin: 10 }} onChange={this.handleOnChange} />
                            </Col>
                            <Col md={9}>
                                <Button style={{ float: "right", margin: 10 }} color="success">Add Product</Button>
                            </Col>
                        </Row>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <DataTable
                                    data={productNotActive}
                                    noHeader={true}
                                    columns={
                                        [
                                            {
                                                name: 'Email',
                                                selector: 'email',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Name',
                                                selector: 'name',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Phone Number',
                                                selector: 'phone',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Vechile Number',
                                                selector: 'vechileNumber',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Vechile Type',
                                                selector: 'vechileType',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Bank Account',
                                                selector: 'bankAccount',
                                                sortable: false,
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (
                                                    <Button>
                                                        <Link to={`/drivers/${dataDummy.id}`}>Approve</Link>
                                                    </Button>
                                                )
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (
                                                    <Button>
                                                        <Link to={`/drivers/${dataDummy.id}`}>Edit</Link>
                                                    </Button>
                                                )
                                            },
                                        ]}
                                    pagination={true}
                                />
                            </TabPane>
                            <TabPane tabId="2">
                                <DataTable
                                    data={productActive}
                                    noHeader={true}
                                    columns={
                                        [
                                            {
                                                name: 'Email',
                                                selector: 'email',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Name',
                                                selector: 'name',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Phone Number',
                                                selector: 'phone',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Vechile Number',
                                                selector: 'vechileNumber',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Vechile Type',
                                                selector: 'vechileType',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Bank Account',
                                                selector: 'bankAccount',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Credit',
                                                selector: 'credit',
                                                sortable: false,
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (
                                                    <Button>
                                                        <Link to={`/drivers/${dataDummy.id}`}>Edit</Link>
                                                    </Button>
                                                )
                                            },
                                        ]}
                                    pagination={true}
                                />
                            </TabPane>
                        </TabContent>
                    </CardBody>
                </Card>
            </div>
        )
    }

}

export default Drivers;