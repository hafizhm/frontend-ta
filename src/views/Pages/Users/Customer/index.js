import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Badge, Row, Col, Input } from 'reactstrap';
import dataDummy from 'assets/dummy/customer.json';
import { Link } from 'react-router-dom';

class Customer extends React.Component {

    constructor(props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
        this.state = {
            activeTab: "1",
            filterSearch: ""
        };
    }

    handleActived = () => {
        const dataCustomer = this.handleFilterQuery()
        const data = dataCustomer.filter(dataItem => dataItem.status === "actived")
        return data;
    }

    handleToggle = (tab) => {
        this.setState({
            activeTab: tab
        });
    }

    handleOnChange = (e) => {
        this.setState({
            filterSearch: e.target.value
        })
    }

    handleFilterQuery = () => {
        const { filterSearch } = this.state
        const dataCustomer = dataDummy.filter(
            dataItem => dataItem.email.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1 || dataItem.fullname.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1)
        return dataCustomer
    }

    render() {
        const customerActived = this.handleActived();

        return (
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Customer</CardHeader>
                    <CardBody>
                        <Row>
                            <Col md={4}>
                                <Input placeholder="Search..." style={{ margin: 10 }} onChange={this.handleOnChange} />
                            </Col>
                        </Row>
                        <DataTable
                            data={customerActived}
                            noHeader={true}
                            columns={
                                [
                                    {
                                        name: 'Email',
                                        selector: 'email',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Full Name',
                                        selector: 'fullname',
                                        sortable: true,
                                    },
                                    {
                                        name: 'Phone Number',
                                        selector: 'phone',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Register Date',
                                        selector: 'registerDate',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Status',
                                        selector: 'status',
                                        sortable: false,
                                        cell: dataDummy => (
                                            <Badge color="primary" style={{ padding: 8 }}>{dataDummy.status}</Badge>
                                        )
                                    },
                                    {
                                        name: '',
                                        selector: 'year',
                                        sortable: true,
                                        cell: dataDummy => (
                                            <Button>
                                                <Link to={`/customer/${dataDummy.id}`}>Detail</Link>
                                            </Button>
                                        )
                                    },
                                ]}
                            pagination={true}
                        />
                    </CardBody>
                </Card>
            </div>
        )
    }
}
export default Customer;