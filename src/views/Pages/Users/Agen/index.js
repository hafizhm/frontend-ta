import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Card, CardHeader, CardBody, Button, Modal, ModalBody, ModalFooter, ModalHeader, Nav, NavItem, NavLink, TabContent, TabPane, Row, Col, Input } from 'reactstrap';
import dataDummy from 'assets/dummy/agen.json';
import { Link } from 'react-router-dom';
import axios from 'axios';

class Agen extends React.Component {

    constructor(props) {
        super(props);
        this.handleToggle = this.handleToggle.bind(this);
        this.state = {
            activeTab: "1",
            filterSearch: "",
            users: [],
            pendingUsers: [],
            modal: false,
            isEditUser: false,
            newUser: {
                email: '',
                phone: '',
                fullName: '',
                firstName: '',
                lastName: '',
                roles: [
                    "RESELLER"
                ],
            }
        }
        this.toggle = this.toggle.bind(this);
    }
    componentDidMount() {
        this.getAgen('/user?roles=7', 'users');
        this.getAgen('/user?roles=8', 'pendingUsers');
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }

    handleToggle = (tab) => {
        const { activeTab } = this.state
        this.setState({
            activeTab: tab
        });
    }

    handleFilterActive = () => {
        const dataAgen = this.handleFilterQuery()
        const data = dataAgen.filter(dataItem => dataItem.statusAgen === "active")
        return data;
    }

    handleFilterNotActive = () => {
        const data = dataDummy.filter(dataItem => dataItem.statusAgen === "pending")
        return data;
    }

    handleOnChange = (e) => {
        this.setState({
            filterSearch: e.target.value
        })
    }

    handleFilterQuery = () => {
        const { filterSearch } = this.state
        const dataAgen = dataDummy.filter(
            dataItem => dataItem.email.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1 || dataItem.name.toLowerCase().indexOf(filterSearch.toLowerCase()) !== -1)
        return dataAgen
    }
    getAgen = (url, state) => {
        axios.get(url)
            .then((result) => {
                // console.log(result);
                const { content } = result.data.data;
                this.setState({
                    [state]: content
                })
            })
    }

    submit = () => {
        const { newUser } = this.state
        // if (!newUser.firstName || !newUser.email || !newUser.phone) {
        //     return alert('isi cukk');
        // }
        axios.post('/user', {
            newUser
        }).then((result, state) => {
            // console.log(result);
            const { content } = result.data.data;
            this.setState({
                [state]: content
            })
        }).catch(error => {
            console.log(`error!`, error);
        })

    }

    onInputChange = (event) => {
        const { name, value } = event.target;
        // console.log(name, value);
        this.setState(previous => {
            return {
                newUser: {
                    ...previous.newUser,
                    [name]: value
                }
            }
        })
    }


    render() {
        const productActive = this.handleFilterActive();
        const productNotActive = this.handleFilterNotActive();
        const { activeTab, users, pendingUsers, newUser, isEditUser } = this.state;
        // console.log(newUser);

        return (
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Agen</CardHeader>
                    <CardBody>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    active={activeTab === "1"}
                                    onClick={() => this.handleToggle("1")}>
                                    Pending
                          </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink active={activeTab === "2"}
                                    onClick={() => this.handleToggle("2")}>
                                    ALL
                          </NavLink>
                            </NavItem>
                        </Nav>
                        <Row>
                            <Col md={4}>
                                <Input placeholder="Search..." style={{ margin: 10 }} onChange={this.handleOnChange} />
                            </Col>
                            <Col md={8}>
                                <Button color="success" onClick={this.toggle} style={{ width: 100, float: "right", margin: 10 }}>Add Agen</Button>
                                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                                    <ModalHeader>{isEditUser ? 'Edit Agen' : 'Add Agen'}</ModalHeader>
                                    <ModalBody>
                                        <Row>
                                            <Col>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td style={{ padding: 10 }}>:</td>
                                                            <td>
                                                                <Input name={users ? users.email : ""} defaultValue={newUser.email} onChange={e => this.onInputChange(e)} />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fullname</td>
                                                            <td style={{ padding: 10 }}>:</td>
                                                            <td>
                                                                <Input name='fullName' defaultValue={newUser.fullName} onChange={e => this.onInputChange(e)} />
                                                            </td>
                                                        </tr>
                                                        {/* <tr>
                                                            <td>Lastname</td>
                                                            <td style={{ padding: 10 }}>:</td>
                                                            <td>
                                                                <Input name='lastname' defaultValue={newUser.lastName} onChange={e => this.onInputChange(e)} />
                                                            </td>
                                                        </tr> */}
                                                        <tr>
                                                            <td>Phone Number</td>
                                                            <td style={{ padding: 10 }}>:</td>
                                                            <td>
                                                                <Input name='phone' defaultValue={newUser.phone} onChange={e => this.onInputChange(e)} />
                                                            </td>
                                                        </tr>

                                                        {/* <tr>
                                                            <td style={{ paddingTop: 20 }}>
                                                                <label>Status<span style={{ padding: "0px 20px" }}>:</span></label>
                                                                <input type="radio" name="check" value="ACTIVE" />
                                                                <label for="active" style={{ marginLeft: 5 }}>Active</label>
                                                                <input type="radio" name="check" value="NOTACTIVE" style={{ marginLeft: 25 }} />
                                                                <label for="notactive" style={{ marginLeft: 5 }}>NotActive</label>
                                                            </td>
                                                        </tr> */}
                                                    </tbody>
                                                </table>
                                            </Col>
                                        </Row>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button onClick={this.submit} color="success" style={{ width: 100 }}>Save</Button>
                                        <Button onClick={this.toggle} color="secondary" style={{ width: 100, marginLeft: 10 }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                {/* </Button> */}
                            </Col>
                        </Row>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <DataTable
                                    data={pendingUsers}
                                    noHeader={true}
                                    columns={
                                        [
                                            {
                                                name: 'Email',
                                                selector: 'email',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Name',
                                                selector: 'firstName',
                                                sortable: true,
                                                cell: user => {
                                                    return `${user.firstName} ${user.lastName}`
                                                }
                                            },
                                            {
                                                name: 'Phone Number',
                                                selector: 'phone',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Register Date',
                                                selector: 'registerDate',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Total Order',
                                                selector: 'totalOrder',
                                                sortable: true,
                                            },

                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (

                                                    <Link to={`/agen/${dataDummy.id}`}>Order</Link>

                                                )
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (

                                                    <Link style={{ color: "primary" }} to={`/agen/${dataDummy.id}`}>Approve</Link>

                                                )
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => (

                                                    <Link style={{ color: "primary" }} to={`/agen/${dataDummy.id}`}>Reject</Link>

                                                )
                                            },
                                        ]}
                                    pagination={true}
                                />
                            </TabPane>
                            <TabPane tabId="2">
                                <DataTable
                                    data={users}
                                    noHeader={true}
                                    columns={
                                        [
                                            {
                                                name: 'Email',
                                                selector: 'email',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Name',
                                                selector: 'firstName',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Phone Number',
                                                selector: 'phone',
                                                sortable: false,
                                            },
                                            {
                                                name: 'Register Date',
                                                selector: 'registerDate',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Total Order',
                                                selector: 'totalOrder',
                                                sortable: true,
                                            },
                                            {
                                                name: 'Point',
                                                selector: 'point',
                                                sortable: true,
                                            },
                                            {
                                                name: '',
                                                selector: 'year',
                                                sortable: true,
                                                cell: dataDummy => {
                                                    // console.log(dataDummy.id)
                                                    return (
                                                        <Link style={{ color: "primary" }} to={`/agen/edit/${dataDummy.id}`}>Edit</Link>

                                                    )
                                                }
                                            },

                                        ]}
                                    pagination={true}
                                />
                            </TabPane>
                        </TabContent>
                    </CardBody>
                </Card>
            </div>
        )
    }

}

export default Agen;