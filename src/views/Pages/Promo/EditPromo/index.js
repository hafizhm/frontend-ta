import React from 'react';
import dataDummy from 'assets/dummy/product.json'
import { Button, Card, CardHeader, CardBody, Row, Col, Input } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

export default props => {

    const [promo, setPromo] = React.useState([]);

    React.useEffect(() =>{
        getPostAPI();
    },[])

    const getPostAPI = () => {
        axios.get('/promo-code')
        .then((result) => {
            console.log(result)
            setPromo(result.data.content)
        })
    }


        // const {promoFile} = this.state;
        // const id = this.props.match.params.id
        // const data = promoFile.filter(dataItem => dataItem.id == id)
        // const promoData = data[0]
        // console.log(promoData)


        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Edit Promo</CardHeader>
                    <CardBody>
                        <Row>
                            <Col>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Kode Promo</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Promo</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Deskripsi</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input type="textarea"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Mulai Promo</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input type="date" id="date-input" name="date-input" placeholder="date" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Berakhir Promo</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input type="date" id="date-input" name="date-input" placeholder="date" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Quota/Banyaknya Promo</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input type="number"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nominal Potongan Promo(Rp)</td>
                                        <td style={{padding: 8}}></td>
                                        <td>
                                            <Input type="number"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tipe Promo</td>
                                        <td style={{padding: 8}}>:</td>
                                        <td>
                                            <Input type="select">
                                                <option value="" defaultChecked>Pilih Tipe Promo</option>
                                                <option value="semua">Semua</option>
                                                <option value="exclusive">Exclusive</option>
                                                <option value="marketplace">Marketplace</option>
                                            </Input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <input type="radio" name="check" value="ACTIVE" checked={promo.status === "ACTIVE"}/>
                                            <label for="active" style={{marginLeft: 5}}>Active</label>
                                        <input type="radio" name="check" value="NOTACTIVE" style={{marginLeft: 5}} checked={promo.status === "NOTACTIVE"}/>
                                            <label for="notactive" style={{marginLeft: 5}}>NotActive</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                                <Button color="success" onClick={() => alert('sukses')} style={{border: "none", marginTop:25}}>Save</Button>
                                <Button color="secondary" style={{marginLeft: 10, marginTop: 25, border: "none"}}>Cancel</Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }