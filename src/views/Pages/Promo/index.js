import React from 'react';
import { Link, Switch } from 'react-router-dom';
import dataDummy from 'assets/dummy/product.json'
import { Button, Badge, Card, CardHeader, CardBody, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Input, Label } from 'reactstrap';
import DataTable from 'react-data-table-component';
import { AppSwitch } from '@coreui/react'
import axios from 'axios';
import Moment from 'react-moment';

export default props => {

    const [promo, setPromo] = React.useState([]);

    React.useEffect(() =>{
        getPostAPI();
    },[])

    const getPostAPI = () => {
        axios.get('/promo-code')
        .then((result) => {
            console.log(result)
            setPromo(result.data.content)
        })
    }

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         modal: false
    //     }

    //     this.toggleAdd = this.toggleAdd.bind(this);
    // }

    // toggleAdd = () =>{
    //     this.setState({
    //         modal: !this.state.modal
    //     })
    // }
    
    
    // handleBanks = () =>{
    //     const data = dataDummy.filter(dataItem => dataItem.reference === "false")
    //     return data;
    // }

        return(
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Promo</CardHeader>
                    <CardBody>
                        <Button color="success">Add Promo</Button>
                        <Modal >
                            <ModalHeader>Add Promo</ModalHeader>
                            <ModalBody>
                                <Row>
                                    <Col>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Kode Promo</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Promo</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Deskripsi</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input type="textarea"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Mulai Promo</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input type="date" id="date-input" name="date-input" placeholder="date" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Berakhir Promo</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input type="date" id="date-input" name="date-input" placeholder="date" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Quota/Banyaknya Promo</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input type="number"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Nominal Potongan Promo(Rp)</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input type="number"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tipe Promo</td>
                                                    <td style={{padding: 8}}>:</td>
                                                    <td>
                                                        <Input type="select">
                                                            <option value="" defaultChecked>Pilih Tipe Promo</option>
                                                            <option value="semua">Semua</option>
                                                            <option value="exclusive">Exclusive</option>
                                                            <option value="marketplace">Marketplace</option>
                                                        </Input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={{paddingTop: 20}}>
                                                    <label>Status<span style={{padding: "0px 20px"}}>:</span></label>
                                                    <input type="radio" name="check" value="ACTIVE"/>
                                                        <label for="active" style={{marginLeft: 5}}>Active</label>
                                                    <input type="radio" name="check" value="NOTACTIVE" style={{marginLeft: 25}}/>
                                                        <label for="notactive" style={{marginLeft: 5}}>NotActive</label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter>
                                <Button  color="success" style={{width: 100}}>Save</Button>
                                <Button  color="secondary" style={{width: 100, marginLeft: 10}}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                        
                        <DataTable
                            noHeader={true}
                            data={promo}
                            striped={true}
                            columns={
                                [
                                    {
                                        name: 'Kode Promo',
                                        selector: 'id',
                                        sortable: true,
                                    },
                                    {
                                        name: 'Nama Promo',
                                        selector: 'name',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Deskripsi',
                                        selector: 'description',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Tanggal Mulai Promo',
                                        selector: 'startDate',
                                        sortable: false,
                                        cell: promo => (
                                            <Moment format="D MMM YYYY" date={promo.startDate}/>
                                        )
                                    },
                                    {
                                        name: 'Tanggal Berakhir Promo',
                                        selector: 'finishDate',
                                        sortable: false,
                                        cell: promo => (
                                            <Moment format="D MMM YYYY" date={promo.finishDate}/>
                                        )
                                    },
                                    {
                                        name: 'Quota/Banyaknya Promo',
                                        selector: 'quota',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Nominal Potongan Promo(Rp)',
                                        selector: 'value',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Tipe Promo',
                                        selector: 'trxType',
                                        sortable: false,
                                    },
                                    {
                                        name: 'Status',
                                        selector: 'status',
                                        sortable: true,
                                        cell: promo => (
                                            <div>
                                                {
                                                    promo.status === true ? (
                                                        <Badge style={{padding: "8px 19px"}} color="success">ACTIVE</Badge>
                                                    )
                                                    :
                                                    (
                                                        <Badge style={{padding: 8}} color="secondary">NOTACTIVE</Badge>
                                                    )
                                                }
                                            </div>
                                        )
                                    },
                                    {
                                        name: 'Action',
                                        selector: '',
                                        sortable: false,
                                        cell: promo => (
                                            <div>
                                                <Link to={`/promo/${promo.id}/edit`} style={{textDecoration: "none"}}>Edit</Link>
                                                <span> | </span>
                                                <Link style={{textDecoration: "none", color: "red"}} onClick={()=> alert('hapus')}>Delete</Link>
                                            </div>
                                        )
                                    }
                                ]}
                            />
                    </CardBody>
                </Card>
            </div>
        )
    }