import Login from './Login';
import Page404 from './Page404';
import Page500 from './Page500';
import Register from './Register';
import Products from './Exclusive/Product';
import Orders from './Orders'
import Ewallet from './Ewallet';
import Banks from './Banks';
import Promo from './Promo';

export {
  Login, Page404, Page500, Register, Products, Orders, Ewallet, Banks, Promo
};