import React from 'react';

const Breadcrumbs = React.lazy(() => import('./views/Base/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/Base/Cards'));
const Carousels = React.lazy(() => import('./views/Base/Carousels'));
const Collapses = React.lazy(() => import('./views/Base/Collapses'));
const Dropdowns = React.lazy(() => import('./views/Base/Dropdowns'));
const Forms = React.lazy(() => import('./views/Base/Forms'));
const Jumbotrons = React.lazy(() => import('./views/Base/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/Base/ListGroups'));
const Navbars = React.lazy(() => import('./views/Base/Navbars'));
const Navs = React.lazy(() => import('./views/Base/Navs'));
const Paginations = React.lazy(() => import('./views/Base/Paginations'));
const Popovers = React.lazy(() => import('./views/Base/Popovers'));
const ProgressBar = React.lazy(() => import('./views/Base/ProgressBar'));
const Switches = React.lazy(() => import('./views/Base/Switches'));
const Tables = React.lazy(() => import('./views/Base/Tables'));
const Tabs = React.lazy(() => import('./views/Base/Tabs'));
const Tooltips = React.lazy(() => import('./views/Base/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/Buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/Buttons/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/Buttons/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/Buttons/Buttons'));
const Charts = React.lazy(() => import('./views/Charts'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/Icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/Icons/Flags'));
const FontAwesome = React.lazy(() => import('./views/Icons/FontAwesome'));
const SimpleLineIcons = React.lazy(() => import('./views/Icons/SimpleLineIcons'));
const Alerts = React.lazy(() => import('./views/Notifications/Alerts'));
const Badges = React.lazy(() => import('./views/Notifications/Badges'));
const Modals = React.lazy(() => import('./views/Notifications/Modals'));
const Colors = React.lazy(() => import('./views/Theme/Colors'));
const Typography = React.lazy(() => import('./views/Theme/Typography'));
const Widgets = React.lazy(() => import('./views/Widgets/Widgets'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

// Halaman Baru
const Product = React.lazy(() => import('./views/Pages/Exclusive/Product'));
const DetailProduct = React.lazy(() => import('./views/Pages/Exclusive/Product/DetailProduct'));
const AddProduct = React.lazy(() => import('./views/Pages/Exclusive/Product/AddProduct'));
const EditProduct = React.lazy(() => import('./views/Pages/Exclusive/Product/DetailProduct/EditProduct'));
const ProductNomenclature = React.lazy(() => import('./views/Pages/Exclusive/ProductNomenclature'));
const EditNomenclature = React.lazy(() => import('./views/Pages/Exclusive/ProductNomenclature/EditNomenclature'));
const Warehouse = React.lazy(() => import('./views/Pages/Exclusive/Warehouse'));
const Orders = React.lazy(() => import('./views/Pages/Orders'));
const DetailOrders = React.lazy(() => import('views/Pages/Orders/Detail Orders'));
const Ewallet = React.lazy(() => import('./views/Pages/Ewallet'));
const Agen = React.lazy(() => import('./views/Pages/Users/Agen'));
const Customer = React.lazy(() => import('./views/Pages/Users/Customer'));
const Drivers = React.lazy(() => import('./views/Pages/Users/Drivers'));
const Supplier = React.lazy(() => import('./views/Pages/Users/Supplier'));
const Banks = React.lazy(() => import('./views/Pages/Banks'));
const EditBanks = React.lazy(() => import('./views/Pages/Banks/EditBanks'));
const Promo = React.lazy(() => import('./views/Pages/Promo'));
const EditPromo = React.lazy(() => import('./views/Pages/Promo/EditPromo'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', exact: true, name: 'Theme', component: Colors },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', exact: true, name: 'Base', component: Cards },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/forms', name: 'Forms', component: Forms },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  // Path Baru
  { path: '/product', exact: true, name: 'Product', component: Product },
  { path: '/product/add', exact: true, name: 'Add Product', component: AddProduct },
  { path: '/product/:id', exact: true, name: 'Detail Product', component: DetailProduct },
  { path: '/product/edit/:id', exact: true, name: 'Edit Product', component: EditProduct },
  { path: '/productnomenclature', exact: true, name: 'Product Nomenclature', component: ProductNomenclature },
  { path: '/productnomenclature/:id/edit', exact: true, name: 'Edit Nomenclature', component: EditNomenclature },
  { path: '/warehouse', exact: true, name: 'Warehouse', component: Warehouse },
  { path: '/orders', exact: true, name: 'Orders', component: Orders },
  { path: '/orders/:id', exact: true, name: 'Detail Orders', component: DetailOrders },
  { path: '/ewallet', exact: true, name: 'Ewallet', component: Ewallet },
  { path: '/agen', exact: true, name: 'Agen', component: Agen },
  { path: '/customer', exact: true, name: 'Customer', component: Customer },
  { path: '/drivers', exact: true, name: 'Drivers', component: Drivers },
  { path: '/supplier', exact: true, name: 'Supplier', component: Supplier },
  { path: '/banks', exact: true, name: 'Banks', component: Banks },
  { path: '/banks/edit/:id', exact: true, name: 'Edit Banks', component: EditBanks },
  { path: '/promo', exact: true, name: 'Promo', component: Promo },
  { path: '/promo/:id/edit', exact: true, name: 'Edit Promo', component: EditPromo },
];

export default routes;
