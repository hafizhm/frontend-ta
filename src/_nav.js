export default {
  items: [
    {
      title: true,
      name: 'Menu Etanee',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Orders',
      url: '/orders',
      icon: 'icon-grid',
    },
    {
      name: 'Users',
      icon: 'icon-people',
      children: [
        {
          name: 'Agen',
          url: '/agen',
        },
        {
          name: 'Customer',
          url: '/customer',
        },
        {
          name: 'Drivers',
          url: '/drivers',
        },
        {
          name: 'Supplier',
          url: '/supplier',
        },
      ]
    },
    {
      name: 'Exclusive',
      icon: 'icon-basket',
      children: [
        {
          name: 'Product',
          url: '/product',
        },
        {
          name: 'Product Nomenclature',
          url: '/productnomenclature',
        },
        {
          name: 'Warehouse',
          url: '/warehouse',
        },
      ]
    },
    {
      name: 'Ewallet',
      url: '/ewallet',
      icon: 'icon-wallet',
    },
    {
      name: 'Banks',
      url: '/banks',
      icon: 'icon-briefcase',
    },
    {
      name: 'Promo',
      url: '/promo',
      icon: 'icon-tag',
    },
  ],
};
